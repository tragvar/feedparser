//
//  UIView+FeedParser.swift
//  FeedParser
//
//  Created by Illya on 2/12/17.
//  Copyright © 2017 Tragvar. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    class func nib() -> UINib {
        return UINib(nibName: self.className(), bundle: nil)
    }
    
    
}
