//
//  NSObjec+FeedParser.swift
//  FeedParser
//
//  Created by Illya on 2/12/17.
//  Copyright © 2017 Tragvar. All rights reserved.
//

import Foundation

extension NSObject {
    class func className() -> String {
        let className = (NSStringFromClass(self) as String).components(separatedBy: ".").last! as String
        return className
    }
    
    func className() -> String {
        let className = (NSStringFromClass(self.classForCoder) as String).components(separatedBy: ".").last! as String
        return className
    }  
}
