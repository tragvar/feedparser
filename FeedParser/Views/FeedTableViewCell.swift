//
//  FeedTableViewCell.swift
//  FeedParser
//
//  Created by Illya on 2/11/17.
//  Copyright © 2017 Tragvar. All rights reserved.
//

import UIKit

class FeedTableViewCell: UITableViewCell {

    static let height: CGFloat = 64.0

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
    }
    
    func configWith(data: FeedItem) {
        titleLabel.text = data.title
        detailsLabel.text = data.details!.stringByDecodingHTMLEntities()
        dateLabel.text = data.pubDate?.formattedPubDateString()
    }
}

