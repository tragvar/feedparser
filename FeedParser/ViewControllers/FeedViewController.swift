//
//  FeedViewController.swift
//  FeedParser
//
//  Created by Illya on 2/11/17.
//  Copyright © 2017 Tragvar. All rights reserved.
//

import UIKit

let url = URL(string: "https://feeds2.feedburner.com/brandontreb")
let kGenerator = "generator"
let kTitle = "title"
let kLink = "link"
let kDescription = "description"
let kPubDate = "pubDate"
let kGuid = "guid"

class FeedViewController: UIViewController {

    @IBOutlet weak var ibTabelView: UITableView!
    
    var entryTitle: String!
    var entryDescription: String!
    var entryLink: String!
    var entryPub: String!
    var entryGenerator: String!

    var currentParsedElement:String! = String()
    var entryDictionary: [String:String]! = Dictionary()
    var datasourse = [FeedItem]()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.ibTabelView.register(FeedTableViewCell.nib(), forCellReuseIdentifier: FeedTableViewCell.className())
        
        if let url = url {
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    print("error : \(error.debugDescription)")
                } else {
                    if let usableData = data {
                        let parser = XMLParser(data: usableData)
                        parser.delegate = self
                        parser.parse()
                    }
                }
            }
            task.resume()
        }
    }

}

// MARK: UITableViewDataSource, UITableViewDelegate

extension FeedViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell : FeedTableViewCell = tableView.dequeueReusableCell(withIdentifier: FeedTableViewCell.className()) as! FeedTableViewCell
        cell.configWith(data: datasourse[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return FeedTableViewCell.height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return datasourse.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tableView.deselectRow(at: indexPath, animated: false)
        if let url = URL(string: datasourse[indexPath.row].link ?? "") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }

}


//MARK: XMLParserDelegate

extension FeedViewController : XMLParserDelegate {
    
    func parser(_ parser: XMLParser!, didStartElement elementName: String, namespaceURI: String?, qualifiedName: String?, attributes attributeDict: [AnyHashable: Any]!) {
        
        
        if elementName == kGenerator {
            entryGenerator = String()
            currentParsedElement = kGenerator
        }
        if elementName == kTitle {
            entryTitle = String()
            currentParsedElement = kTitle
        }
        if elementName == kLink {
            entryLink = String()
            currentParsedElement = kLink
        }
        if elementName == kPubDate {
            entryPub = String()
            currentParsedElement = kPubDate
        }
        if elementName == kDescription {
            entryDescription = String()
            currentParsedElement = kDescription
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        
        if currentParsedElement == kGenerator {
            entryGenerator = entryGenerator + string
        }
        if currentParsedElement == kTitle {
            entryTitle = entryTitle + string
        }
        if currentParsedElement == kLink {
            entryLink = entryLink + string
        }
        if currentParsedElement == kPubDate {
            entryPub = entryPub + string
        }
        if currentParsedElement == kDescription {
            entryDescription = entryDescription + string
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String!, namespaceURI: String!, qualifiedName qName: String!) {
        
        if elementName == kTitle {
            entryDictionary[kTitle] = entryTitle
        }
        if elementName == kLink {
            entryDictionary[kLink] = entryLink
        }
        if elementName == kPubDate {
            entryDictionary[kPubDate] = entryPub
        }
        if elementName == kDescription {
            entryDictionary[kDescription] = entryDescription
        }
        if elementName == kGenerator {
            self.title = entryDictionary[kTitle]
        }
        
        if elementName == kGuid {
            let feedItem = FeedItem()
            feedItem.title = entryDictionary[kTitle]
            feedItem.link = entryDictionary[kLink]
            feedItem.pubDate = entryDictionary[kPubDate]
            feedItem.details = entryDictionary[kDescription]
            datasourse.append(feedItem)
        }
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        
        DispatchQueue.main.async(execute: { () -> Void in
            self.ibTabelView.reloadData()
        })
    }
}




