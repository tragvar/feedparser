//
//  FeedItem.swift
//  FeedParser
//
//  Created by Illya on 2/12/17.
//  Copyright © 2017 Tragvar. All rights reserved.
//

import Foundation

class FeedItem: NSObject {

    var title: String?
    var link: String?
    var details: String?
    var pubDate: String?
    
    // MARK: - init
    convenience init(title: String, url: String, details: String, pubDate: String) {
        self.init()
        self.title = title
        self.link = url
        self.details = details.stringByDecodingHTMLEntities()
        self.pubDate = pubDate
    }
    
    
}
